﻿Описать XPath'ы на первых двух страницах ("1. Выбор полиса" и "2. Оформление") для:

-   Каждой кнопки;
-   Каждого поля для ввода текста;
-   Каждого чекбокса;
-   Каждого датапикера;
-   Логотипа "СБЕР СТРАХОВАНИЕ";
-   Слайдера в блоке выбора суммы;
-   Хедера "Что будет застраховано?";
-   Текстовых блоков: "Мебель, техника и ваши вещи", "Падение летательных аппаратов и их частей";
-   Каждой колонки в списке, который находится под хедером "Страховая защита включенная в программу".

_P.S.: XPath'ы нужно писать для каждого элемента отдельно_

## XPath'ы для каждой кнопки (всего 11 **button**)

1. Кнопка «Квартира»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[1]/div/div[1]/mat-button-toggle/button

Или

//button[@id="mat-button-toggle-1-button"]

2. Кнопка «Дом»:

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[1]/div/div[2]/mat-button-toggle

Или

//*[@id="mat-button-toggle-2"]

3. Календарь

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[3]/div/mat-form-field/div/div[1]/div[4]/mat-datepicker-toggle/button

Или

//button[@aria-label="Open calendar"]

#### Кнопки «Да» и «Нет» (сдаются в аренду)

4.   «Да»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[2]/div[2]/div[1]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-22-button"]

5. «Нет»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[2]/div[2]/div[2]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-23-button"]

Кнопки «Да» и «Нет» (расположена на первом или последнем этаже)

6. «Да»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[3]/div[2]/div[1]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-25-button"]

7. «Нет»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[3]/div[2]/div[2]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-26-button"]

#### Кнопки «Да» и «Нет» (установлена охранная сигнализация)

8. «Да»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[4]/div[2]/div[1]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-28-button"]

9. «Нет»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/mat-button-toggle-group[4]/div[2]/div[2]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-29-button"]

10. Применить:

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[9]/div[2]/div/button

Или

//button[span=" Применить "]

11. Оформить

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[2]/button

Или

//button[span=" Оформить "]

### XPath'ы для каждого поля для ввода текста (всего 4 input):

1. Регион проживания

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[1]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-0"]

2. Срок действия страхования (календарь)

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[3]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-1"]

3. Страховая сумма и объекты страхования (выбор суммы)

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/div/input

Или

//*[@id="mat-input-3"]

4. Ввод  текста  промокод

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[9]/div[1]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-2"]

### XPath'ы для каждого чек-бокса

На странице выбора полиса нет чек-боксов

### XPath'ы для datepicker (календарь) 

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[3]/div/mat-form-field/div/div[1]/div[3]/mat-datepicker

Или

//mat-datepicker[@class="ng-tns-c61-1"]  

(не оригинальный путь, т.к. нет id, если добавят еще календарь с таким же классом поиск покажет оба datepicker)

### Логотипа "СБЕР СТРАХОВАНИЕ"

/html/body/app-root/header/div/div

Или

//*[@id="header"]/div/div

### Слайдер в блоке выбора суммы

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[4]/div/app-input-range/div[1]/mat-slider

Или

//mat-slider[@role="slider"]

### Хедер «Что будет застраховано»

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/h4[1]

Или

//*[text()="Что будет застраховано?"]

**Текстовые блоки: "Мебель, техника и ваши вещи", "Падение летательных аппаратов и их частей**

1. Стены и перекрытия:*

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[5]/div[1]

Или

//*[text()=" Стены и перекрытия "]

2. Мебель, техника и ваши вещи

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[6]/div[1]

Или

//*[text()=" Мебель, техника и ваши вещи "]

3. Ремонт, электрика, сантехника, трубы, окна, двери

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[7]/div[1]

Или

//*[text()=" Ремонт, электрика, сантехника, трубы, окна, двери "]

4. Ответственность перед соседями

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[8]/div[1]

Или

//*[text()=" Ответственность перед соседями "]

5.  Чрезвычайная ситуация

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[1]/div/div

Или

//*[text()="Чрезвычайная ситуация"]

6.  Механическое воздействие

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[2]/div/div

Или

//*[text()="Механическое воздействие"]

7.  Удар молнии

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[3]/div/div

Или

//*[text()="Удар молнии"]

8.  Взрыв

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[4]/div/div

Или

//*[text()="Взрыв"]

9.  Залив

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul/li[5]/div/div

Или

//*[text()="Залив"]

10.  Стихийные бедствия

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[1]/div/div

Или

//*[text()="Стихийные бедствия"]

11.  Противоправные действия третьих лиц

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[2]/div/div

Или

//*[text()="Противоправные действия третьих лиц"]

12.  Падение посторонних предметов

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[3]/div/div

Или

//*[text()="Падение посторонних предметов"]

13.  Падение летательных аппаратов и их частей

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[4]/div/div

Или

//*[text()="Падение летательных аппаратов и их частей"]

14.  Пожар

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul/li[5]/div/div

Или

//*[text()="Пожар"]

### Колонки в списке, который находится под хедером "Страховая защита включенная в программу".

**Левая колонка**

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[1]/ul

**Правая колонка**

/html/body/app-root/div/app-policy-form/div/div/app-policy-calculation-form/form/div[1]/div[2]/div[2]/ul

### Выбор полиса:

/html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[1]

Или

//*[text()="1. Выбор полиса"]

### Защита квартиры и дома онлайн:

/html/body/app-root/div/app-policy-form/app-policy-form-header/div/div

Или

//*[text()="Защита квартиры и дома онлайн"]

## Страница Оформление

**Оформление****:**

/html/body/app-root/div/app-policy-form/div/app-policy-form-stepper/div[1]/div[2]/p

Или

//*[text()="2. Оформление"]

//mat-checkbox

### XPath'ы для каждой кнопки (всего 7 button)

1.Заполнить по СберID

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[1]/button

Или

//*[text()=" Заполнить по Сбер ID "]

2. Календарь в дате рождения

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[4]/div/mat-form-field/div/div[1]/div[4]/mat-datepicker-toggle/button

Или

//button[@aria-label="Open  calendar"]  

(не уникальный поиск: найдутся 2 кнопки календаря на странице)

3. Кнопка выбора пола (Мужской)

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/mat-button-toggle-group/div[1]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-58-button"]

4. Кнопка выбора пола (Женский)

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/mat-button-toggle-group/div[2]/mat-button-toggle/button

Или

//*[@id="mat-button-toggle-59-button"]

5. Кнопка календаря в дате выдачи:

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[1]/div[2]/div/mat-form-field/div/div[1]/div[4]/mat-datepicker-toggle/button

Или

//button[@aria-label="Open  calendar"] 

 (не уникальный поиск: найдутся 2 кнопки календаря на странице)

6. Кнопка Вернуться

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[5]/button[1]

Или

//button[span[text()="Вернуться"]]

7. Кнопка Оформить

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[5]/button[2]

Или

//button[span[text()="Оформить"]]

### XPath'ы для каждого поля ввода (всего 18 input-ов)

1. Поле Фамилия

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[2]/div[1]/mat-form-field/div/div[1]/div[3]/input

или

//*[@id="mat-input-4"]

2. Поле Имя

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[2]/div[2]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-5"]

3. Поле Отчество

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[3]/div[1]/mat-form-field/div/div[1]/div[3]/input

или

//*[@id="mat-input-6"]

4. Поле чек-бокса "Отчество отсутствует"

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[3]/div[2]/mat-checkbox/label/div/input

Или

//*[@id="mat-checkbox-1-input"]

5. Поле "Дата рождения"

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[4]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-7"]

6. Поле Серия

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[1]/div[1]/div[1]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-8"]

7. Поле Номер

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[1]/div[1]/div[2]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-9"]

8. Поле  "Дата  выдачи"

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[1]/div[2]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-10"]

9. Поле Код подразделения

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[3]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-12"]

10. Поле регион (Адрес имущества)

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[1]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-13"]

11. Поле Город или населенный пункт

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[2]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-14"]

12. Поле Улица

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[3]/div[1]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-15"]

13. Поле Улица отсутствует

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[3]/div[2]/mat-checkbox/label/div/input

Или

//*[@id="mat-checkbox-2-input"]

14. Поле Дом, литера, корпус, строение

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[4]/div[1]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-16"]

15. Поле Квартира

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[4]/div[2]/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-17"]

16. Телефон (Контактные данные страхователя)

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[4]/div[1]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-18"]

17. Поле электронная почта

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[4]/div[2]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-19"]

18. Поле Повтор электронной почты

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[4]/div[3]/div/mat-form-field/div/div[1]/div[3]/input

Или

//*[@id="mat-input-20"]

### XPath'ы для каждого чек-бокса (2  mat-checkbox)

1. Чек-бокс "Отчество отсутствует":

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[3]/div[2]/mat-checkbox

Или

//*[@id="mat-checkbox-1"]

2. Чек-бокс "Улица отсутствует"

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[3]/div[3]/div[2]/mat-checkbox

Или

//*[@id="mat-checkbox-2"]

### XPath'ы для каждого датапикера (всего 2 mat-datepicker)

1. Датапикер дата рождения (Контейнер Страхователь):

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[1]/div[4]/div/mat-form-field/div/div[1]/div[3]/mat-datepicker

Или

//mat-datepicker[@class="ng-tns-c61-6"]

2. Датапикер "дата выдачи"

/html/body/app-root/div/app-policy-form/div/div/app-policy-contact-form/form/div/div[2]/div[1]/div[2]/div/mat-form-field/div/div[1]/div[3]/mat-datepicker

Или

//mat-datepicker[@class="ng-tns-c61-9"]
